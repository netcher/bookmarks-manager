# Bookmarks manager
 - Build an application which manages bookmarks.
 - Scaffold the application with Angular 8, using the following libraries: o NGRX for managing the state o Angular material for the user interface
 - Define the store for the application. Each bookmark should have the following properties: o Name o URL o Group (work / leisure/ personal / ...)
 - Create the user inteface for displaying the bookmarks groupped by the &#34;group&#34; property.
 - Enhance the existing application by adding the following new features: o Add new bookmarks using a form o Delete bookmarks by clicking in the list

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.20.

## Quick start
1) Run `npm install` in the project root to install dependencies.

2) Run `npm start` for a dev server. 

3) Navigate to `http://localhost:4200/`

## Screenshot of working solution
![App screenshot](https://image.prntscr.com/image/1fPTagFLSMi1fnuJffwaVg.png)

Link: https://prnt.sc/q8hfn2

## Description of the approach
 - "Containers and Presentational components" approach was used to build the project structure
 - NgRx Store was used for a state management tool
 - NgRx Store was slightly enhanced with a Facade Pattern to simplify its usage across the project
 - Angular Material was used as a Design Framework
 - Clicking on the bookmark would open a link in a new tab, clicking DELETE button would remove it from the list
