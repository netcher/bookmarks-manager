import {Component} from '@angular/core';
import {BookmarksStore} from "./store/bookmarks.store";
import {IBookmark} from "./interfaces/bookmarks.interface";
import {map} from "rxjs/operators";
import {groupBy} from 'lodash-es';

@Component({
  selector: 'app-bookmarks',
  templateUrl: './bookmarks.component.html',
  styleUrls: ['./bookmarks.component.scss']
})
export class BookmarksComponent {
  bookmarks$ = this.bookmarksStore.select$('data')
    .pipe(map(data => groupBy(data, 'group')));

  constructor(private readonly bookmarksStore: BookmarksStore) {}

  openPage(url: string): void {
    window.open(url, '_blank');
  }

  deleteItem(id: string): void {
    this.bookmarksStore.dispatch(new this.bookmarksStore.actions.Remove(id));
  }

  addItem(item: IBookmark): void {
    this.bookmarksStore.dispatch(new this.bookmarksStore.actions.Add(item));
  }
}
