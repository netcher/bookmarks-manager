import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {BookmarksComponent} from "./bookmarks.component";
import {COMPONENTS} from "./components";
import {BookmarksStoreModule} from "./store/bookmarks.module";
import {ReactiveFormsModule} from "@angular/forms";
import {
  MatButtonModule, MatCardModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatListModule
} from "@angular/material";


@NgModule({
  declarations: [
    BookmarksComponent,
    ...COMPONENTS
  ],
  imports: [
    BrowserModule,
    BookmarksStoreModule,
    ReactiveFormsModule,

    // Material
    MatButtonModule,
    MatListModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatCardModule
  ],
  exports: [
    BookmarksComponent
  ],
  bootstrap: [BookmarksComponent]
})
export class BookmarksModule {
}
