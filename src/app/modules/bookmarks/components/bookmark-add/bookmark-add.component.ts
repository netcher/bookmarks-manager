import {Component, EventEmitter, Output, ViewEncapsulation} from '@angular/core';
import {IBookmark} from "../../interfaces/bookmarks.interface";
import {FormBuilder, Validators} from "@angular/forms";
import {TBookmark} from "../../types/bookmark.enum";

@Component({
  selector: 'app-bookmark-add',
  templateUrl: './bookmark-add.component.html',
  styleUrls: ['./bookmark-add.component.scss'],
  encapsulation: ViewEncapsulation.Emulated
})
export class BookmarkAddComponent {

  @Output()
  addItem = new EventEmitter<IBookmark>();

  formGroup = this.formBuilder.group({
    name: ['', Validators.required],
    url: ['', [Validators.required, Validators.pattern('(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?')]],
    group: [TBookmark.Personal, Validators.required]
  });

  constructor(private readonly formBuilder: FormBuilder) {}

  onSubmit(): void {
    if (this.formGroup.valid) {
      this.addItem.emit(this.formGroup.value);
    }
  }
}
