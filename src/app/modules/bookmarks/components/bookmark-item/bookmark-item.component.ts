import {Component, EventEmitter, Input, Output, ViewEncapsulation} from '@angular/core';
import {IBookmark} from '../../interfaces/bookmarks.interface';

@Component({
  selector: 'app-bookmark-item',
  templateUrl: './bookmark-item.component.html',
  styleUrls: ['./bookmark-item.component.scss'],
  encapsulation: ViewEncapsulation.Emulated
})
export class BookmarkItemComponent {

  @Input()
  bookmark: IBookmark;

  @Output()
  onOpenPage = new EventEmitter<string>();

  @Output()
  onDelete = new EventEmitter<string>();

  openPage(url: string): void {
    this.onOpenPage.emit(url);
  }

  delete($event, id: string): void {
    $event.stopPropagation();
    this.onDelete.emit(id);
  }
}
