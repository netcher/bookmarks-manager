import {BookmarkItemComponent} from "./bookmark-item/bookmark-item.component";
import {BookmarkAddComponent} from "./bookmark-add/bookmark-add.component";

export const COMPONENTS = [
  BookmarkItemComponent,
  BookmarkAddComponent
];
