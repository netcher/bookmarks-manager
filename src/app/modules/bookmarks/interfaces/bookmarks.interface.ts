import {TBookmark} from "../types/bookmark.enum";

export interface IBookmark {
  id?: string;
  name: string;
  url: string;
  group: TBookmark;
}
