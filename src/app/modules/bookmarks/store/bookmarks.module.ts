import {NgModule} from '@angular/core';
import {EffectsModule} from '@ngrx/effects';
import {StoreModule} from '@ngrx/store';
import {BookmarksStore} from "./bookmarks.store";
import {BookmarksService} from "./bookmarks.service";
import {BookmarksReducer} from "./redux/bookmarks.reducer";
import {BookmarksEffects} from "./redux/bookmarks.effects";

@NgModule({
  imports: [
    StoreModule.forFeature(BookmarksStore.storeName, BookmarksReducer.reducer, {
      initialState: BookmarksReducer.initialState
    }),
    EffectsModule.forFeature([BookmarksEffects])
  ],
  providers: [
    BookmarksService,
    BookmarksStore
  ]
})
export class BookmarksStoreModule {
}
