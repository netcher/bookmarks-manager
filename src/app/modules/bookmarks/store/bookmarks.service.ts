import {Injectable} from "@angular/core";
import {Observable, of} from "rxjs";
import {TBookmark} from "../types/bookmark.enum";
import {IBookmark} from "../interfaces/bookmarks.interface";
import * as uuid from 'uuid';

@Injectable({providedIn: 'root'})
export class BookmarksService {

  /**
   * Simulates initial bookmarks data
   */
  initData(): Observable<IBookmark[]> {
    return of([
      {
        id: uuid.v4(),
        name: 'Google Search',
        url: '//google.com',
        group: TBookmark.Work
      },
      {
        id: uuid.v4(),
        name: 'Facebook Search',
        url: '//facebook.com',
        group: TBookmark.Personal
      },
      {
        id: uuid.v4(),
        name: 'Github',
        url: '//github.com',
        group: TBookmark.Work
      }
    ])
  }

  /**
   * Adds uuid to the item
   */
  buildItem(item: IBookmark): Observable<IBookmark> {
    return of({
      ...item,
      id: uuid.v4()
    })
  }
}
