import {Injectable} from '@angular/core';
import {Action, select, Store} from '@ngrx/store';
import {Observable} from 'rxjs';
import {IBookmarksState} from "./redux/bookmarks.state";
import {BookmarksActions} from "./redux/bookmarks.actions";

@Injectable()
export class BookmarksStore {

  static storeName = 'bookmarks';

  readonly actions = BookmarksActions;

  constructor(protected readonly store: Store<{[key: string]: IBookmarksState}>) {}

  select$<T extends keyof IBookmarksState>(field: T): Observable<IBookmarksState[T]> {
    return this.store.pipe(select(BookmarksStore.storeName, field));
  }

  dispatch(action: Action): void {
    this.store.dispatch(action);
  }
}
