import {Action} from '@ngrx/store';
import {IBookmark} from "../../interfaces/bookmarks.interface";

export namespace BookmarksActions {

  export enum TActions {
    Init = '[Bookmarks] Init',
    InitSuccess = '[Bookmarks] Init Success',
    Add = '[Bookmarks] Add item',
    AddSuccess = '[Bookmarks] Add item success',
    Remove = '[Bookmarks] Remove item',
    Fail = '[Bookmarks] Some error happened',
  }

  export class Init implements Action {
    readonly type = TActions.Init;
  }

  export class InitSuccess implements Action {
    readonly type = TActions.InitSuccess;

    constructor(readonly payload: IBookmark[]) {
    }
  }

  export class Add implements Action {
    readonly type = TActions.Add;

    constructor(readonly payload: IBookmark) {
    }
  }

  export class AddSuccess implements Action {
    readonly type = TActions.AddSuccess;

    constructor(readonly payload: IBookmark) {
    }
  }

  export class Remove implements Action {
    readonly type = TActions.Remove;

    constructor(readonly payload: string) {
    }
  }

  export class Fail implements Action {
    readonly type = TActions.Fail;
  }

  export type TAll = Init | InitSuccess | Add | AddSuccess | Remove | Fail;
}
