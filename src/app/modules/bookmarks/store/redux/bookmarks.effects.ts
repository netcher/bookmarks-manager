import {Injectable} from '@angular/core';
import {Actions, Effect, ofType, OnInitEffects} from '@ngrx/effects';
import {of} from 'rxjs';
import {catchError, map, switchMap} from 'rxjs/operators';
import {BookmarksActions} from "./bookmarks.actions";
import {BookmarksService} from "../bookmarks.service";
import {Action} from "@ngrx/store";

@Injectable()
export class BookmarksEffects implements OnInitEffects {

  constructor(private readonly actions$: Actions,
              private readonly bookmarksService: BookmarksService) {
  }

  ngrxOnInitEffects(): Action {
    return new BookmarksActions.Init();
  }

  @Effect()
  init$ = this.actions$
    .pipe(ofType<BookmarksActions.Init>(BookmarksActions.TActions.Init))
    .pipe(switchMap((action) => this.bookmarksService.initData()
      .pipe(map(res => new BookmarksActions.InitSuccess(res)))
      .pipe(catchError(() => of(new BookmarksActions.Fail())))
    ));

  @Effect()
  add$ = this.actions$
    .pipe(ofType<BookmarksActions.Add>(BookmarksActions.TActions.Add))
    .pipe(switchMap((action) => this.bookmarksService.buildItem(action.payload)
      .pipe(map(res => new BookmarksActions.AddSuccess(res)))
      .pipe(catchError(() => of(new BookmarksActions.Fail())))
    ));
}
