import {BookmarksActions} from "./bookmarks.actions";
import {IBookmarksState} from "./bookmarks.state";

export namespace BookmarksReducer {

  export const initialState: IBookmarksState = {
    data: null
  };

  export function reducer(state: IBookmarksState, action: BookmarksActions.TAll): IBookmarksState {
    switch (action.type) {
      case BookmarksActions.TActions.InitSuccess: {
        return {
          ...state,
          data: action.payload
        };
      }

      case BookmarksActions.TActions.Remove: {
        return {
          ...state,
          data: state.data.filter((item) => item.id !== action.payload)
        };
      }

      case BookmarksActions.TActions.AddSuccess: {
        return {
          ...state,
          data: [...state.data, action.payload]
        };
      }

      case BookmarksActions.TActions.Fail: {
        return {
          ...state
        };
      }
    }

    return state;
  }
}
