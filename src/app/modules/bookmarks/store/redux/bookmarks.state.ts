import {IBookmark} from '../../interfaces/bookmarks.interface';

export interface IBookmarksState {
  data: IBookmark[];
}
