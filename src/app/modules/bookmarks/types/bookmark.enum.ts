export enum TBookmark {
  Work = 'WORK',
  Leisure = 'LEISURE',
  Personal = 'PERSONAL'
}
